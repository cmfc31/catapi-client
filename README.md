# CatApi Client

**Developer:** Martin Fuentes 🚀


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

## Live demo 
This project was deployed on Vercel with CI/CD and SSR support. Check the live demo available at: https://catapi-client.vercel.app

## Tech stack 
This project is made with latest Node LTS version (v16.15.1). If you need to 
handle multiple node.js versions on your machine I recommend to use Node Version 
Manager tool like: [nvm-windows](https://github.com/coreybutler/nvm-windows).

- Vue.js v2.6.x
- Nuxt v2.15.8
- @nuxtjs/tailwindcss
- @nuxtjs/axios
- Font Awesome 6
- Vue Toastification

### Integrated linter
Default ESLint rules configured in dedicated config files.
- ESLint + Prettier

### Recommend VS Code extensions

- [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur)
- [Vue VSCode Snippets](https://marketplace.visualstudio.com/items?itemName=sdras.vue-vscode-snippets)
- [PostCSS Language Support](https://marketplace.visualstudio.com/items?itemName=csstools.postcss)
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

### Screenshots
![Preview 1](https://dl.dropbox.com/s/20sylp66jp8yeg4/Screenshot_1.png "Preview 1")
![Preview 2](https://dl.dropbox.com/s/g234bf0f0e147w9/Screenshot_2.png "Preview 2")
![Preview 3](https://dl.dropbox.com/s/k5coliv707ig1fc/Screenshot_3.png "Preview 3")
