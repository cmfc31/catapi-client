export default function ({ $axios, $config, redirect }) {
  $axios.onRequest((config) => {
    config.headers.common['x-api-key'] = $config.apiKey
  })
}
